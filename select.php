<?php

/**
 * 选择排序算法示例
 *
 * @author ShuangYa
 * @link https://git.oschina.net/phpbar/sort
 * @license https://git.oschina.net/phpbar/sort/blob/master/LICENSE
 */

//待排序数组
$arr = [5, 9, 10, 67, 22, 98, 34, 66, 71, 12, 2, 53];
//排序结果
$result = [];
//取得数组长度
$length = count($arr);
for ($i = 0; $i < $length; $i++) {
	//初始化：设置为数组的第一个
	reset($arr);
	$min = current($arr);
	$minKey = key($arr);
	foreach ($arr as $k => $v) {
		//获取$arr中最小的数
		if ($v < $min) {
			$min = $v;
			$minKey = $k;
		}
	}
	//将最小的数插入到$result中
	$result[] = $min;
	unset($arr[$minKey]);
}

//输出
print_r($result);