<?php

/**
 * 冒泡排序算法示例
 *
 * @author ShuangYa
 * @link https://git.oschina.net/phpbar/sort
 * @license https://git.oschina.net/phpbar/sort/blob/master/LICENSE
 */

//待排序数组
$arr = [5, 9, 10, 67, 22, 98, 34, 66, 71, 12, 2, 53];
//取得数组长度
$length = count($arr);
//慢慢降低每次需要搜索的数量
for ($i = $length; $i > 0; $i--) {
	//从第一个数搜索到第$i-1个元素
	for ($j = 0; $j < ($i - 1); $j++) {
		if ($arr[$j] > $arr[$j + 1]) {
			//交换两个数
			$temp = $arr[$j];
			$arr[$j] = $arr[$j + 1];
			$arr[$j + 1] = $temp;
		}
	}
}

//输出
print_r($arr);