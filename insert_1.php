<?php

/**
 * 插入排序算法：直接插入法示例
 *
 * @author ShuangYa
 * @link https://git.oschina.net/phpbar/sort
 * @license https://git.oschina.net/phpbar/sort/blob/master/LICENSE
 */

//待排序数组
$arr = [5, 9, 10, 67, 22, 98, 34, 66, 71, 12, 2, 53];
//初始化
$result = [];
$result[] = $arr[0];
$length = count($arr);
for ($i = 1; $i < $length; $i++) {
	for ($j = count($result); $j >= 0; $j--) {
		//寻找$arr[$i]应该插入的位置
		if (isset($result[$j - 1]) && $result[$j - 1] > $arr[$i]) {
			$result[$j] = $result[$j - 1];
		} else {
			$result[$j] = $arr[$i];
			break;
		}
	}
}

//输出
print_r($result);